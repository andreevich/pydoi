package ru.startandroid.develop.p1051fragmentdynamic;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MyService extends Service {
	Boolean flag = true;
	Boolean flag2 = false;
	public DBHelper dbHelper;
	public SQLiteDatabase db;
	public List<String> lst = new ArrayList<String>();
	public String subject = null;
	// public Mail m;

	final String LOG_TAG = "myLogsService";

	public void onCreate() {
		super.onCreate();
		Log.d(LOG_TAG, "onCreate");
		dbHelper = new DBHelper(this);
		db = dbHelper.getReadableDatabase();
	}

	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d(LOG_TAG, "onStartCommand");
		if (!flag2)
			someTask();

		return super.onStartCommand(intent, flags, startId);
	}

	public void onDestroy() {
		super.onDestroy();
		flag = false;
		flag2 = false;
		db.close();
		Log.d(LOG_TAG, "onDestroy");
	}

	public IBinder onBind(Intent intent) {
		Log.d(LOG_TAG, "onBind");
		return null;
	}

	/*
	 * ������ � ������ TimeUnit.SECONDS.sleep(1); = 1 �������.
	 */
	void someTask() {
		flag = true;
		flag2 = true;
		new Thread(new Runnable() {
			public void run() {
				while (flag) {
					maintask();
					try {
						TimeUnit.SECONDS.sleep(2);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// stopSelf();
				}
			}
		}).start();
	}

	void maintask() {

		/*
		 * �� ������� �������� ������ �� �� � ������ � ��� -------------------
		 */
		try {
			lst.clear();
			String[] columns = { "0" };
			// columns[0]= "1";

			Cursor c = db.query("zayvka", null, "status = ?", columns, null,
					null, null);
			if (c.moveToFirst()) {
				int idColIndex = c.getColumnIndex("id");
				int ID_s = c.getColumnIndex("ID_s");
				int type_mes = c.getColumnIndex("type_mes");
				int mes_a_ColIndex = c.getColumnIndex("MESSAGE_q");

				do {

					if (c.getString(type_mes).equals("PODHOD")) {

						subject = "[zevs]"
								+ "[hash:c70ef269920f051248497d79914ba2ba13de3199]"
								+ // ����� �� �������� ���
								"[podhod:" + c.getString(mes_a_ColIndex) + "]"
								+ "[ID:" + c.getString(ID_s) + "]";

					}

					lst.add(subject);

					ContentValues cv = new ContentValues();
					cv.put("status", "1");
					int updCount = db.update("zayvka", cv, "id = ?",
							new String[] { c.getInt(idColIndex) + "" });
					Log.d("--", updCount + "");

				} while (c.moveToNext());
			} else {
			}
			c.close();
			Log.d(LOG_TAG, lst.toString());

		} catch (Exception exep) {

			Log.d(">>", "�������� � ����� " + exep);
			stopSelf();
		} finally {
		}
		/*
		 * ------------------------------------------------------------------
		 */
	}
}
