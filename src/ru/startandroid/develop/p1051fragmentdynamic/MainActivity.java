package ru.startandroid.develop.p1051fragmentdynamic;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class MainActivity extends Activity {

	Fragment1 frag1;
	FragmentMenu fm1;
	IOmenuFragment iomf1;
	FragmentTransaction fTrans;

	AbDialog ab;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		iomf1 = new IOmenuFragment();

		fm1 = new FragmentMenu();
		frag1 = new Fragment1();
		fTrans = getFragmentManager().beginTransaction();
		fTrans.add(R.id.frgmMenu, fm1);
		fTrans.add(R.id.frgmCont, frag1);
		fTrans.commit();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		ActionBar actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		Log.d("--->", item.getItemId() + "");
		fTrans = getFragmentManager().beginTransaction();
		switch (item.getItemId()) {

		case 2131296284: // IN/OUT

			fTrans.replace(R.id.frgmMenu, iomf1);
			startService(new Intent(this, MyService.class)); // ������ �������
			break;

		case 2131296285: // AboutBox
			ab = new AbDialog();
			ab.show(getFragmentManager(), "ab");
			stopService(new Intent(this, MyService.class)); // ��������� �������
			break;

		case android.R.id.home: // ����� �� �������
			// FragmentMenu fm2 = new FragmentMenu();
			fTrans.replace(R.id.frgmMenu, fm1);
			// fTrans.replace(R.id.frgmCont, frag1);
			// Log.d("--->","Home button");
			break;
		default:
			break;
		}

		fTrans.addToBackStack(null);
		fTrans.commit();

		return false;
	}

	/*
	 * public void onClick(View v) { fTrans =
	 * getFragmentManager().beginTransaction(); switch (v.getId()) { case
	 * R.id.btnAdd: fTrans.add(R.id.frgmCont, frag1); break; case
	 * R.id.btnRemove: fTrans.remove(frag1); break; case R.id.btnReplace:
	 * fTrans.replace(R.id.frgmCont, frag2); default: break; } // if
	 * (chbStack.isChecked()) fTrans.addToBackStack(null); fTrans.commit(); }
	 */

}