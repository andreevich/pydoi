package ru.startandroid.develop.p1051fragmentdynamic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

public class AbDialog extends DialogFragment implements OnClickListener {

	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
				.setTitle("� ����������:")
				// .setPositiveButton(R.string.yes, this)
				// .setNegativeButton(R.string.no, this)
				.setMessage(R.string.message_ab_text)
				.setIcon(R.drawable.ic_launcher);
		return adb.create();
	}

	public void onClick(DialogInterface dialog, int which) {
		int i = 0;
		switch (which) {
		case Dialog.BUTTON_POSITIVE:
			i = R.string.yes;
			break;
		case Dialog.BUTTON_NEGATIVE:
			i = R.string.no;
			break;
		}
	}

	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
	}

	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
	}
}
