/*
 * 
 * Fragment1.java - ����� ��� ��������� ��������� � ��������� �� ������ � ��� �����:
 * ������ �������, ���� � �������, ������� ������...
 * 
 * 
 */
package ru.startandroid.develop.p1051fragmentdynamic;

import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;

public class Fragment1 extends Fragment {
	DialogFragment dlg1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.fragment1, null);

		/*
		 * ��������� ������ ������ � ���� ��� �����: ���������� ������ �
		 * �������� � ���������, �������� - ����� ������� � ������ ������� �
		 * ���! � ��� ��� ������!
		 */

		ImageButton button1 = (ImageButton) v.findViewById(R.id.imageButton1);
		button1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dlg1 = new Dialog1();
				dlg1.show(getFragmentManager(), "dlg1");
			}
		});

		return v;
	}

}