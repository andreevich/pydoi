/*
 * ������ MainList.java - ������������ ��������
 * 
 */
package ru.startandroid.develop.p1051fragmentdynamic;

import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FragmentMenu extends ListFragment {

	String data[] = new String[] { "��� �����", "����", "��. ��", "ZEUS app" };

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_activated_1, data);
		setListAdapter(adapter);

		Log.d("-->", "�������� ����!");

		FragmentTransaction fTrans2;

		fTrans2 = getFragmentManager().beginTransaction();

		Fragment1 frag1 = new Fragment1();
		fTrans2.replace(R.id.frgmCont, frag1);

		fTrans2.commit();
	}

	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		FragmentTransaction fTrans2;

		fTrans2 = getFragmentManager().beginTransaction();

		switch (position) {
		case 0:
			Fragment1 frag1 = new Fragment1();
			fTrans2.replace(R.id.frgmCont, frag1);
			break;
		case 1:
			Fragmenr2 frag2 = new Fragmenr2();
			fTrans2.replace(R.id.frgmCont, frag2);
			break;

		default:
			break;
		}
		fTrans2.commit();

	}

}
