package ru.startandroid.develop.p1051fragmentdynamic;

/*	 Mail's Subject
 * 
 *  [zevs][hash:c70ef269920f051248497d79914ba2ba13de3199][podhod:140009][ID:20130323122616000300]
 */

public class IOitem {
	int id;
	String data_o;
	String data_i;
	String ID_s;
	String MESSAGE_q;
	String status;
	String prioritet;
	String MESSAGE_a;
	String type_mes;
	String hash = "c70ef269920f051248497d79914ba2ba13de3199";

	IOitem(int id, String data_o, String data_i, String ID_s, String MESSAGE_q,
			String status, String prioritet, String MESSAGE_a) {
		this.id = id;
		this.data_o = data_o;
		this.data_i = data_i;
		this.ID_s = ID_s;
		this.MESSAGE_q = MESSAGE_q;
		this.status = status;
		this.prioritet = prioritet;
		this.MESSAGE_a = MESSAGE_a;
	}

	IOitem(int id, String MESSAGE_q, String MESSAGE_a, String type_mes,
			String id_s, String status) {
		this.id = id;
		this.MESSAGE_q = MESSAGE_q;
		this.MESSAGE_a = MESSAGE_a;
		this.type_mes = type_mes;
		this.ID_s = id_s;
		this.status = status;
	}

	public int getID() {
		return this.id;
	}

	public String getMes_a() {
		return this.MESSAGE_a;
	}

	public String getMes_q() {
		return this.MESSAGE_q;
	}

	public String typeMes() {
		return this.type_mes;
	}

	public String log() {
		return "id: " + this.id + "; " + "MESSAGE_q: " + this.MESSAGE_q + "; "
				+ "TYPE_m: " + this.type_mes + "; " + "\nID: " + this.ID_s
				+ "; " + "\nhash: " + this.hash + ";" + "\nstatus: "
				+ this.status + "; " + "\nMESSAGE_a: " + this.MESSAGE_a + "; ";
	}
}
