package ru.startandroid.develop.p1051fragmentdynamic;

import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Dialog1 extends DialogFragment {

	final String LOG_TAG = "myLogs";
	public int pos_ = 0;
	public String name_sel = null;
	Spinner spinner;
	DBHelper dbHelper;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		dbHelper = new DBHelper(this.getActivity());

		getDialog().setTitle("������ ������� �...");
		View v = inflater.inflate(R.layout.dialog1, null);

		/*
		 * � data ��������� ������ ������� (���-�������) �� ������� ��
		 * (��������� ���)
		 */
		Resources res = getResources();
		String[] stations = res.getStringArray(R.array.stations);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				this.getActivity(), android.R.layout.simple_spinner_item,
				stations);
		adapter.setDropDownViewResource(android.R.layout.simple_list_item_activated_1);
		spinner = (Spinner) v.findViewById(R.id.spinner);
		spinner.setAdapter(adapter);

		spinner.setSelection(297); // ������ ���-1

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				int item = spinner.getSelectedItemPosition();
				String name = spinner.getItemAtPosition(arg2).toString()
						.substring(0, 6);
				// Log.d("-->",item+" ");
				pos_ = item;
				name_sel = name;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		Button button = (Button) v.findViewById(R.id.btnYes);
		button.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Toast.makeText(getActivity(), name_sel, Toast.LENGTH_LONG)
						.show();

				// ��������� ID ��� �������. 20 ��������.
				DateFormat dateFormat = new SimpleDateFormat(
						"yyyyMMddHHmmssSSSSSS");
				Date date = new Date();

				SQLiteDatabase db = dbHelper.getWritableDatabase();

				ContentValues cv = new ContentValues();
				Log.d(LOG_TAG, "--- Insert in zayvka: ---");
				cv.put("data_o", "1");
				cv.put("data_i", "2");
				cv.put("ID_s", dateFormat.format(date));
				cv.put("MESSAGE_q", name_sel);
				cv.put("status", "0");
				cv.put("prioritet", "0");
				cv.put("type_mes", "PODHOD");
				cv.put("MESSAGE_a", " ");

				long rowID = db.insert("zayvka", null, cv);
				Log.d(LOG_TAG, "row inserted, ID = " + rowID);
				Log.d(LOG_TAG, "--- Rows in zayvka: ---");

				Cursor c = db.query("zayvka", null, null, null, null, null,
						null);

				if (c.moveToFirst()) {
					int idColIndex = c.getColumnIndex("id");
					int mesColIndex = c.getColumnIndex("MESSAGE_q");
					int idnColIndex = c.getColumnIndex("ID_s");

					do {
						// �������� �������� �� ������� �������� � ����� ��� �
						// ���
						Log.d(LOG_TAG, "ID = " + c.getInt(idColIndex)
								+ ", Mes = " + c.getString(mesColIndex)
								+ ", email = " + c.getString(idnColIndex));
					} while (c.moveToNext());
				} else
					Log.d(LOG_TAG, "0 rows");
				c.close();

				/*
				 * Log.d(LOG_TAG, "--- Clear mytable: ---"); // ������� ���
				 * ������ int clearCount = db.delete("zayvka", null, null);
				 * Log.d(LOG_TAG, "deleted rows count = " + clearCount);
				 */
				dbHelper.close();

				dismiss();
			}
		});

		Button button2 = (Button) v.findViewById(R.id.btnNo);
		button2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dismiss();
			}
		});

		RadioGroup segmentRadioGroup = (RadioGroup) v
				.findViewById(R.id.radioGroup1);

		segmentRadioGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup radioGroup, int i) {
						switch (i) {
						case 2131296259: // ������ �������
							spinner.setSelection(297);

							break;
						case 2131296260: // ������ �������
							spinner.setSelection(229);

							break;
						case 2131296261: // ������ �������
							spinner.setSelection(0);

							break;
						case 2131296262:
							spinner.setSelection(499);

							break;
						case 2131296263:
							spinner.setSelection(694);

							break;
						case 2131296264:
							spinner.setSelection(783);

							break;
						}
						// showMap();
					}
				});

		return v;

	}

	/*
	 * 
	 * 
	 * public void onClick(View v) { Log.d(LOG_TAG, "Dialog 1: " + ((Button)
	 * v).getText()); //Log.d(LOG_TAG, "Dialog 1: " +
	 * v1.findViewById(R.id.spinner);
	 * 
	 * dismiss(); }
	 * 
	 * public void onDismiss(DialogInterface dialog) { super.onDismiss(dialog);
	 * Log.d(LOG_TAG, "Dialog 1: onDismiss"); Log.d(LOG_TAG,"--> "+pos_+" w"); }
	 * 
	 * public void onCancel(DialogInterface dialog) { super.onCancel(dialog);
	 * Log.d(LOG_TAG, "Dialog 1: onCancel"); }
	 */
}