package ru.startandroid.develop.p1051fragmentdynamic;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class IOmenuFragment extends ListFragment {
	public DBHelper dbHelper;
	public SQLiteDatabase db;
	String data[] = new String[] { "������ ������", "������ ������" };
	List<String> lst = new ArrayList<String>();
	public int Item_sel;

	List<IOitem> lst_2 = new ArrayList<IOitem>();

	FragmentTransaction fTrans;
	Fragment_read_podhod read_podhod;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		try {
			dbHelper = new DBHelper(this.getActivity());
			db = dbHelper.getReadableDatabase();
			lst.clear();
			Cursor c = db.query("zayvka", null, null, null, null, null, null);
			if (c.moveToFirst()) {
				int idColIndex = c.getColumnIndex("id");
				int mesColIndex = c.getColumnIndex("MESSAGE_q");
				int mes_a_ColIndex = c.getColumnIndex("MESSAGE_a");
				int type_m = c.getColumnIndex("type_mes");
				int id_s = c.getColumnIndex("ID_s");
				int status = c.getColumnIndex("status");

				do {
					lst.add(c.getInt(idColIndex) + " -> "
							+ c.getString(mesColIndex));
					lst_2.add(new IOitem(c.getInt(idColIndex), c
							.getString(mesColIndex), c
							.getString(mes_a_ColIndex), c.getString(type_m), c
							.getString(id_s), c.getString(status)));
				} while (c.moveToNext());
			} else {
				Log.d("!@!@!@!@!@->", "0 rows");
				lst.add("�e� ������");
			}
			c.close();

		} catch (Exception exep) {

			Log.d(">>", "�������� � ����� " + exep);
		} finally {
			db.close();
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_list_item_activated_1,
				lst.toArray(new String[lst.size()]));
		setListAdapter(adapter);

		// setHasOptionsMenu(true); // ���� �� ��� ���������
		registerForContextMenu(getListView());

		read_podhod = new Fragment_read_podhod();
		fTrans = getFragmentManager().beginTransaction();
		fTrans.replace(R.id.frgmCont, read_podhod);
		fTrans.commit();

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, 1, 2, "������� ������");
		menu.add(3, 4, 5, "������ ��������");
		menu.add(0, 5, 5, "Send Mail");
		// menu.add(0,7,5,"������ ��������");

	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item
				.getMenuInfo();

		switch (item.getItemId()) {
		case 1: // ����������� ���� "������� ������"

			// Log.d("������ ���� �� ","� ������: "+acmi.position+" ID:"+lst_2.get(acmi.position).getID());

			int ID = lst_2.get(acmi.position).getID();
			SQLiteDatabase db = dbHelper.getWritableDatabase();
			int clearCount = db.delete("zayvka", "id = " + ID, null);

			// Log.d("������ ",clearCount+" �����"+"; ");
			db.close();

			lst.remove(acmi.position);
			lst_2.remove(acmi.position);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(),
					android.R.layout.simple_list_item_activated_1,
					lst.toArray(new String[lst.size()]));
			setListAdapter(adapter);

			break;
		case 4:
			// Log.d("������ ���� �� >",lst_2.get(acmi.position).log());
			break;

		case 5:
			/*
			 * Properties props = new Properties(); props.put("mail.smtp.com" ,
			 * "smtp.gmail.com"); Session session = Session.getDefaultInstance(
			 * props , null); String to = "alexandr.andreevich@gmail.com";
			 * String from = "5316552@gmail.com";
			 * 
			 * try { Message msg = new MimeMessage(session); msg.setFrom(new
			 * InternetAddress(from)); msg.setRecipient(Message.RecipientType.TO
			 * , new InternetAddress(to)); msg.setSubject("qweqwe");
			 * msg.setText("Working fine..!"); } catch(Exception exc) {
			 * Log.e("Mail","�������� "+exc.toString()); }
			 */
			Mail m = new Mail("5316552@gmail.com", "1simon86");

			String[] toArr = { "alexandr.andreevich@gmail.com" };
			m.setTo(toArr);
			m.setFrom("5316552@gmail.com");
			m.setSubject("Party Booked");
			m.setBody("asasas");

			try {
				// m.addAttachment("/sdcard/filelocation");

				if (m.send()) {
					Toast.makeText(this.getActivity(), "Sent Email.",
							Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(this.getActivity(), "Email was not sent.",
							Toast.LENGTH_LONG).show();
				}
			} catch (Exception e) {
				// Toast.makeText(MailApp.this,
				// "There was a problem sending the email.",
				// Toast.LENGTH_LONG).show();
				Log.e("PartyPlannerActivity", "Could not send email.", e);
			}
			break;
		default:
			break;
		}

		return super.onContextItemSelected(item);
	}

	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		if (lst_2.get(position).typeMes().equals("PODHOD")) { // ���� � ������
																// ������/�����
																// �� ������

			Log.d("� >", lst_2.get(position).log());
			Fragment frag2 = getFragmentManager().findFragmentById(
					R.id.frgmCont);
			((TextView) frag2.getView().findViewById(R.id.textPodhod))
					.setText(lst_2.get(position).log());
		}

	}

}
